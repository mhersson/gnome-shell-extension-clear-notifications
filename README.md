# gnome-shell-extension-clear-notifications
by mhersson

## Install:
```
mkdir -p ~/.local/share/gnome-shell/extensions/clear-notificatoins@mhersson
cp ./extension.js ./metadata.json ~/.local/share/gnome-shell/extensions/clear-notificatoins@mhersson
```

## Usage:
```
gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell/Extensions/ClearNotifications --method org.gnome.Shell.Extensions.ClearNotifications.Call
```
