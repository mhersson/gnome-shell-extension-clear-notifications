// by mhersson
// License: GPLv3
// Install:
// mkdir -p ~/.local/share/gnome-shell/extensions/clear-notifications@mhersson
// cp ./extension.js ./metadata.json ~/.local/share/gnome-shell/extensions/clear-notifications@mhersson
// Usage:
// gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell/Extensions/ClearNotifications --method org.gnome.Shell.Extensions.ClearNotifications.Call

const { Gio, GObject } = imports.gi;
const Main = imports.ui.main;

const MR_DBUS_IFACE = `
<node>
    <interface name="org.gnome.Shell.Extensions.ClearNotifications">
        <method name="Call">
        </method>
    </interface>
</node>`;

const Extension = GObject.registerClass(
class Extension extends GObject.Object {
    enable() {
        this._dbus = Gio.DBusExportedObject.wrapJSObject(MR_DBUS_IFACE, this);
        this._dbus.export(Gio.DBus.session, '/org/gnome/Shell/Extensions/ClearNotifications');
    }

    disable() {
        this._dbus.flush();
        this._dbus.unexport();
        delete this._dbus;
    }

    Call() {
        Main.panel.statusArea.dateMenu._messageList._sectionList.get_children().forEach(s => s.clear());
    }
});

function init() {
    return new Extension();
}
